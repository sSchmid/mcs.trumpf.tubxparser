```

████████╗██╗   ██╗██████╗ ██╗  ██╗██████╗  █████╗ ██████╗ ███████╗███████╗██████╗ 
╚══██╔══╝██║   ██║██╔══██╗╚██╗██╔╝██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔════╝██╔══██╗
   ██║   ██║   ██║██████╔╝ ╚███╔╝ ██████╔╝███████║██████╔╝███████╗█████╗  ██████╔╝
   ██║   ██║   ██║██╔══██╗ ██╔██╗ ██╔═══╝ ██╔══██║██╔══██╗╚════██║██╔══╝  ██╔══██╗
   ██║   ╚██████╔╝██████╔╝██╔╝ ██╗██║     ██║  ██║██║  ██║███████║███████╗██║  ██║
   ╚═╝    ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝
```
---                                                                                  


## Features

### Parsing Files with .tubx Extension
* Right click a `.TUBX`-File and select Command **TubxParser: parse tubx**
* Unzips the file to *%TEMP%/TUBXParser/OriginalFilename/*
* Parses all the Files with .bson Extension in the unzipped folder and writes copies as *filename*.json
* Opens the unpacked files in a new VS-Code Instance

### Parsing Files with .bson Extension (Binary JSON)
* Right click a `.BSON-`File and select Command **TubxParser: parse bson**
* Parses the files and writes a copy to the same path, named *filename*.json

## Requirements
* VSCode =)

## Install this extension
* Download `.VSIX`-File of the latest Version, open Visual Studio Code, navigate to 'Extensions' -> 'Install from VSIX', select `.VSIX`-File and reload Code.

## Extension Settings
This extension contributes the following settings:

* `TUBXParser.OutputInformationLevel`: One of the following Information Levels: off, minimal, extended, verbose
Controls which extension-specific messages are sent to the user.

## Known Issues
At the moment there are no known issues, please create a new [gitlab issue](https://gitlab.com/sSchmid/mcs.trumpf.tubxparser/-/issues) to report newly found problems.

## Features to implement in the future (probably...)
* Reverse of unpacking and parsing operations

## Releases
### V1.0.0
[Download VSIX](https://sschmid.gitlab.io/mcs.trumpf.tubxparser/release/tubxparser-1.0.0.vsix) 

-----------------------------------------------------------------------------------------------------------

## Release Notes

### 0.0.1
* Initial alpha-release

### 1.0.0
* Removed treeview of unpacked TUBX in favor of opening the unpacked files in new a vscode instance
* Added settings to control messages to user (messagebox, status bar, output message)
* Added Icon
* Slightly better documentation