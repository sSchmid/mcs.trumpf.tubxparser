
import * as vscode from 'vscode';
import * as path from 'path';
import * as os from 'os';
import * as fs from 'fs';
import * as BSON from 'bson';
import * as ZIP from '7zip-min';


let statusBarItem: vscode.StatusBarItem;
let outputChannel: vscode.OutputChannel;


/**
 * this method is called when the extension is activated for the first time
 */
export function activate(context: vscode.ExtensionContext) {
  outputChannel = vscode.window.createOutputChannel("TUBXParser");
  statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
  const infoLevel:string = vscode.workspace.getConfiguration('TUBXParser').get('OutputInformationLevel') || 'extended';
  switch(infoLevel){
    case "verbose":
    case "extended":
      // fallthrough
    case "minimal":
      outputChannel.appendLine('Extension "TUBXParser" is now active');
      outputChannel.appendLine('Info Level: ' + infoLevel);
  }


  /**
   * @command 'extension.parseBSON'
   * parse the current BSON-File to human readable JSON
   */
	let disposable = vscode.commands.registerCommand('extension.parseBSON', (uri:vscode.Uri) => {
    const fileList = new Array<string>();
    fileList.push(uri.fsPath);
    parseBsonFiles(fileList);

		// statusBarItem.text = `$(megaphone) BSON successfully parsed`;
    // statusBarItem.show();
    outputMessage(
      "BSON successfully parsed", 
      `TubxParser successfully parsed '${path.basename(uri.fsPath)}' to '${uri.fsPath}.json'`
    );
	});
	context.subscriptions.push(disposable);

  /**
   * @command 'extension.parseTUBX'
   * unpack the given TUBX-File to temp, find all BSON-Files, parse and prettify them
   */
	disposable = vscode.commands.registerCommand('extension.parseTUBX', ( uri:vscode.Uri ) => {

    const tempDir = path.join(os.tmpdir(), 'TUBXParser');
    if( !fs.existsSync(tempDir)){
      fs.mkdirSync(tempDir);
    }
    const tempTubxDir = path.join(tempDir, path.basename(uri.fsPath, '.tubx'));
    if( fs.existsSync(tempTubxDir)){
      deleteFolderRecursive(tempTubxDir);
    }

    outputMessage(`Unpacking TUBX-File at ${uri.fsPath} to ${tempTubxDir}`);
    
    // tubx is essentally a zip-file and can be unpacked
		ZIP.unpack(uri.fsPath, tempTubxDir, (err:string) => {
			if ( err ) {
        outputMessage(`Unpacking TUBX at ${uri.fsPath} produced an error: ${err}`, "", "error");
			} else {
        processUnpackedTubx(uri.fsPath, tempTubxDir);
      }
    });
	});
	context.subscriptions.push(disposable);
}

/**
 * this method is called when the extension is deactivated
 */
export function deactivate() {}


/**
 * read all unpacked files, find bson files, parse and output to temp
 * @param originalTubxFile 
 * @param tubxdir 
 */
function processUnpackedTubx(originalTubxFile:string ,tubxdir: string) {
  const unpackedFiles:Array<string> = [];
  walk(tubxdir, unpackedFiles)
  .then((foundFiles) => parseBsonFiles(foundFiles))
  .then((processedBsonFiles) => {
    vscode.commands.executeCommand("vscode.openFolder", vscode.Uri.file(tubxdir));

    outputMessage(`Parsed TUBX-File at 
      ${originalTubxFile} to ${tubxdir}, \r\n
      deserialized and prettyfied ${processedBsonFiles.length} BSON-Files in the process`,
      `TubxParser successfully parsed '${path.basename(originalTubxFile)}' to '${tubxdir}'`
    );
    
  });
}

/**
 * outputs information message according to settings
 * @param message 
 */
function outputMessage(message:string, notificationMessage:string = "", level:string = "info"){
  const infoLevel:string = vscode.workspace.getConfiguration('TUBXParser').get('OutputInformationLevel') || 'extended';
  switch(infoLevel){
    case "verbose":
      if(notificationMessage.length > 0) {
        vscode.window.showInformationMessage(notificationMessage);
      } else {
        vscode.window.showInformationMessage(message);
      }
      // fallthrough
    case "extended":
      if(level=="info"){
        statusBarItem.text = `$(megaphone) ${message}`;
        statusBarItem.show();
      }
      // fallthrough
    case "minimal":
      outputChannel.appendLine(message);
  }
}

/**
 * remove a folder and all its files
 * @param path 
 */
function deleteFolderRecursive(path:string) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file:string){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}

/**
 * Find all files inside a dir, recursively.
 * @function walk
 * @param  {string} dir Dir path string.
 * @return {string[]} Array with all file names that are inside the directory.
*/
async function walk(dir:string, fileList:Array<string> = []): Promise<string[]> {
  fileList = fileList || new Array<string>();
  const files = await fs.promises.readdir(dir);
  for (const file of files) {
    const stat = await fs.promises.stat(path.join(dir, file));
    if (stat.isDirectory()) {
      fileList = await walk(path.join(dir, file), fileList);
    } else {
      fileList.push(path.join(dir, file));
    }
  }
  return fileList;
}

/**
 * try to parse all the bson-files in the given array
 * @function parseBsonFiles
 * @param  {string} dir Dir path string.
 * @return {string[]} Array with the processed file-paths.
*/
async function parseBsonFiles(fileList:Array<string>): Promise<string[]> {

  //filter list for bson-files 
  const targetFiles = fileList.filter(function(file) {
    return path.extname(file).toLowerCase() === '.bson';
  });
  outputChannel.appendLine(`Parsing BSON-Files: `);

  // parse all bson files and write to temp
  targetFiles.forEach( (file, index, array) => {
    outputChannel.appendLine(`    ${file}`);
    const fileContents = fs.readFileSync(file);
    const json = BSON.deserialize(fileContents);
    const jsonPretty = JSON.stringify(json, null,2);  
    const newFilePath = path.join(path.dirname(file), path.basename(file, '.bson') + '.json' );
    fs.writeFileSync(newFilePath, jsonPretty);
  });
  return targetFiles;
}

