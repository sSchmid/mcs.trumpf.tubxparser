const gulp = require('gulp');
const tslintHtmlReport = require('tslint-html-report');

gulp.task('tslint-report', function() {
    const config = {
        tslint: 'tslint.json', // path to tslint.json
        srcFiles: 'src/**/*.ts', // files to lint
        outDir: 'reports/tslint-html-report', // output folder to write the report to
        html: 'index.html', // name of the html report generated
        exclude: ['src/**/*.ts'], // Files/patterns to exclude
        breakOnError: false, // Should it throw an error in tslint errors are found
        typeCheck: true, // enable type checking. requires tsconfig.json
        tsconfig: 'tsconfig.json' // path to tsconfig.json
      }
  tslintHtmlReport(config);
  return Promise.resolve("Finished Gulp Task 'tslint-report'");
});


 
